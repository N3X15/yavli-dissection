(function() {
  var __a = {};
  ! function(a, b, c) {
    a[b] = c()
  }(__a, "v", function() {
    function a() {
      return {
        width: k(),
        height: l()
      }
    }

    function b(a, b) {
      var c = {};
      return b = +b || 0, c.width = (c.right = a.right + b) - (c.left = a.left - b), c.height = (c.bottom = a.bottom + b) - (c.top = a.top - b), c
    }

    function c(a, c) {
      return a = a && !a.nodeType ? a[0] : a, a && 1 === a.nodeType ? b(a.getBoundingClientRect(), c) : !1
    }

    function d(b) {
      b = null == b ? a() : 1 === b.nodeType ? c(b) : b;
      var d = b.height,
        e = b.width;
      return d = "function" == typeof d ? d.call(b) : d, e = "function" == typeof e ? e.call(b) : e, e / d
    }
    var e = {},
      f = "undefined" != typeof window && window,
      g = "undefined" != typeof document && document,
      h = g && g.documentElement,
      i = f.matchMedia || f.msMatchMedia,
      j = i ? function(a) {
        return !!i.call(f, a).matches
      } : function() {
        return !1
      },
      k = e.viewportW = function() {
        var a = h.clientWidth,
          b = f.innerWidth;
        return b > a ? b : a
      },
      l = e.viewportH = function() {
        var a = h.clientHeight,
          b = f.innerHeight;
        return b > a ? b : a
      };
    return e.mq = j, e.matchMedia = i ? function() {
      return i.apply(f, arguments)
    } : function() {
      return {}
    }, e.viewport = a, e.scrollX = function() {
      return f.pageXOffset || h.scrollLeft
    }, e.scrollY = function() {
      return f.pageYOffset || h.scrollTop
    }, e.rectangle = c, e.aspect = d, e.inX = function(a, b) {
      var d = c(a, b);
      return !!d && d.right >= 0 && d.left <= k()
    }, e.inY = function(a, b) {
      var d = c(a, b);
      return !!d && d.bottom >= 0 && d.top <= l()
    }, e.inViewport = function(a, b) {
      var d = c(a, b);
      return !!d && d.bottom >= 0 && d.right >= 0 && d.top <= l() && d.left <= k()
    }, e
  });
  ! function(n, t, e) {
    t[n] = e()
  }("q", __a, function() {
    function n(n) {
      return [].slice.call(n, 0)
    }

    function t(n) {
      var t;
      return n && "object" == typeof n && (t = n.nodeType) && (1 == t || 9 == t)
    }

    function e(n) {
      return "object" == typeof n && isFinite(n.length)
    }

    function o(n) {
      for (var t = [], o = 0, r = n.length; r > o; ++o) e(n[o]) ? t = t.concat(n[o]) : t[t.length] = n[o];
      return t
    }

    function r(n) {
      var t, e, o = [];
      n: for (t = 0; t < n.length; t++) {
        for (e = 0; e < o.length; e++)
          if (o[e] == n[t]) continue n;
        o[o.length] = n[t]
      }
      return o
    }

    function i(n) {
      return n ? "string" == typeof n ? u(n)[0] : !n[s] && e(n) ? n[0] : n : f
    }

    function u(r, u) {
      var a, s = i(u);
      return s && r ? r === l || t(r) ? !u || r !== l && t(s) && m(r, s) ? [r] : [] : r && e(r) ? o(r) : f.getElementsByClassName && "string" == r && (a = r.match(c)) ? n(s.getElementsByClassName(a[1])) : r && (r.document || r.nodeType && 9 == r.nodeType) ? u ? [] : [r] : n(s.querySelectorAll(r)) : []
    }
    var c = /^\.([\w\-]+)$/,
      f = document,
      l = window,
      a = f.documentElement,
      s = "nodeType",
      m = "compareDocumentPosition" in a ? function(n, t) {
        return 16 == (16 & t.compareDocumentPosition(n))
      } : function(n, t) {
        return t = t == f || t == window ? a : t, t !== n && t.contains(n)
      };
    return u.uniq = r, u
  }, this);
  var c = 0;
  var uc = false;
  var cl = [];
  var pv = false;
  var css = false;
  var later = function() {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
      eventListenerSupported = window.addEventListener;
    return function(o) {
      var obj = __a.q(o.parent)[0];
      if (obj == undefined) {
        return false;
      }
      if (MutationObserver) {
        var obs = new MutationObserver(function(mutations, observer) {
          if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
            scan(o);
          }
        });
        obs.observe(obj, {
          childList: true,
          subtree: true,
        });
        scan(o);
      } else if (eventListenerSupported) {
        obj.addEventListener('DOMNodeInserted', scan, false);
      }
    }
  };
  var scan = function(o) {
    var a = __a.q(o.child, __a.q(o.parent)[0]);
    for (var p = 0; p < a.length; p++) {
      if (cl[p] == undefined) {
        cl[p] = a[p];
        oio.now(oio.ur(oio.u), a[p]);
      }
    }
  };
  var is_test = function() {
    return (window.location.hash.search("__ytest") > -1) ? true : false;
  };
  var acss = function(css) {
    var h = document.getElementsByTagName('head')[0],
      s = document.createElement('style');
    s.setAttribute("type", "text/css");
    if (s.styleSheet) s.styleSheet.cssText = css;
    else s.appendChild(document.createTextNode(css));
    return h.appendChild(s);
  };
  var g = function() {
    var w = window.innerWidth;
    var r = 5;
    if (w <= 479) r = 1;
    if (w <= 767) r = 2;
    if (w <= 1023) r = 3;
    if (w <= 1439) r = 4;
    return r;
  };
  var collection = function(ucl, parent) {
    var o = {
      init: function() {
        if (css === false) {
          css = acss(ucl.css_common);
        }
        for (var i = 0; i < ucl.unit_list.length; i++) {
          o.unit(ucl.unit_list[i], parent);
        }
      },
      unit: function(u, parent) {
        for (i = 0; i < u.exclude_url_list.length; i++) {
          var eu = new RegExp(u.exclude_url_list[i]);
          if (window.location.pathname.match(eu)) return;
        }
        if (u.target_selector != undefined && u.target_selector != "") {
          var ts = u.target_selector,
            ta = u.target_approach,
            cs = u.container_style;
        } else if (is_test() && u.target_selector_test != undefined && u.target_selector_test != "") {
          var ts = u.target_selector_test,
            ta = u.target_approach_test,
            cs = u.container_style_test;
        } else return;
        u.css_specific += "." + u.widget_class + " {" + u.container_style + "}";
        var t = __a.q(ts, parent);
        if (t.length < 1) return;
        t = t[0];
        css.textContent += u.css_specific;
        if (ta == "before") {
          var n = document.createElement("div");
          n.innerHTML = u.html;
          t.parentNode.insertBefore(n, t);
          n.outerHTML = n.innerHTML;
        } else if (ta == "after") {
          var n = document.createElement("div");
          n.innerHTML = u.html;
          t.parentNode.insertBefore(n, t.nextSibling);
          n.outerHTML = n.innerHTML;
        } else if (ta == "append") t.innerHTML = t.innerHTML + u.html;
        else if (ta == "prepend") t.innerHTML = u.html + t.innerHTML;
        else if (ta == "fill") t.innerHTML = u.html;
        u.unit = __a.q("." + u.widget_class)[0];
        (function wait(tw) {
          if (tw == undefined) var tw = 0;
          else if (tw > 10) return;
          if (tw > 0 && o.rig(u) == true) return;
          setTimeout(function() {
            wait(tw + 1);
          }, 100);
        })();
        window.addEventListener("resize", function() {
          o.rig(u);
        });
      },
      rig: function(u) {
        e = __a.q("." + u.widget_class + " ." + u.img_class);
        if (e[0].clientWidth == 0) return false;
        var ratio = e[0].clientWidth / ucl.sprite_width;
        var new_bg_height = Math.floor(ratio * ucl.sprite_height);
        var ad_height = Math.floor(new_bg_height / ucl.sprite_count);
        if (ad_height * ucl.sprite_count != new_bg_height) {
          new_bg_height = ad_height * ucl.sprite_count;
        }
        if (u.once == undefined) {
          u.once = true;
          u.alv = [];
          for (var i = 0; i < e.length; i++) {
            var ad_id = e[i].className.split(u.img_class + "_")[1];
            css.textContent += '.' + u.widget_class + ' .' + u.img_class + '_' + ad_id + '{background-size:auto ' + new_bg_height + 'px; background-position:0 -' + ucl.ad_to_sprite[ad_id] * ad_height + 'px;}';
            if (e[i].offsetParent !== null) {
              u.alv.push(ad_id);
            }
            var d = __a.q("#" + e[i].parentElement.parentElement.parentElement.id + " " + e[i].parentElement.tagName);
            for (p = 0; p < d.length; p++) {
              if (d[p].onclick == null) {
                d[p].onclick = function(ee) {
                  var a = ee.target || ee.srcElement;
                  var ci = a.parentElement.getAttribute("ci");
                  window.location.assign(ucl.link_redirect_url + ucl.ad_to_link[ci]);
                  return false;
                };
              }
            }
          }
          (function rview() {
            if (__a.v.inViewport(u.unit)) {
              var lt = "";
              lt = u.widget_id + "c" + u.alv.join([separator = "c"]);
              var pv = 0;
              if (oio.pv == false) {
                oio.pv = true;
                pv = 1;
              }
              var i = new Image();
              i.src = oio.ur(ucl.view_url, {
                'i': lt,
                'p': pv,
                'g': g(),
                'w': u.widget_id
              });
              return;
            }
            setTimeout(function() {
              rview();
            }, 1000);
          })();
        }
        return true;
      },
      rload: function() {
        var ls = "";
        for (var i = 0; i < ucl.unit_list.length; i++) {
          if (ucl.unit_list[i]["alv"] != undefined) {
            ls += ucl.unit_list[i]["widget_id"] + "c" + ucl.unit_list[i]["alv"].join([separator = "c"]) + "-";
          }
        }
        ls = ls.substr(0, ls.length - 1);
        var lsi = new Image();
        lsi.src = oio.ur(ucl.load_url, {
          'i': ls,
          'g': g()
        });
        return true;
      },
    };
    o.init();
    setTimeout(o.rload, 1000);
    return o;
  };
  var execute = function() {
    if (c == 1 && oio.ucl.observe.enabled == 1) {
      oio.uc = oio.ucl;
      later(oio.ucl.observe);
    } else {
      collection(oio.ucl, oio.p);
    }
  }();
})();
