import os, sys, json

# Requires python-build-tools. pip install -Ue https://gitlab.com/N3X15/python-build-tools
# Also requires js-beautify.
from buildtools import cmd, os_utils
data = {}
with open(sys.argv[1], 'r') as f:
    data = json.load(f)

def dumpTo(data, filename):
    dirname = os.path.dirname(filename)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    print('Dumping to {}...'.format(filename))
    with open(filename, 'w') as f:
        f.write(data)


dumpTo(data['css_common'], os.path.join('dump','common.css'))
dumpTo(data['server_js'],  os.path.join('dump','server.js'))
cmd([os_utils.which('js-beautify'), '-f', os.path.join('dump','server.js'), '-r', '-s', '2', '-q', '-n'], echo=True, show_output=True, critical=True)
for unit in data['unit_list']:
    dumpTo(unit['css_specific'], os.path.join('dump', 'units', str(unit['widget_id']), 'specific.css'))
    dumpTo(unit['css'], os.path.join('dump', 'units', str(unit['widget_id']), 'css.css'))
    dumpTo(unit['html'], os.path.join('dump', 'units', str(unit['widget_id']), 'html.htm'))
