from PIL import Image
import sys, os, json

filename = sys.argv[1]
outfileraw = filename+'.raw.json'
outfilepretty = filename+'.pretty.json'

img = Image.open(filename)
w,h = img.size
# FORMAT: R,G,B,A
'''
p24: function() {
  return function(b) {
    var s = [];
    for (var i = 0; i < b.length; i += 4) {
      if (b[i + 0] > 0) s.push(b[i + 0]);
      if (b[i + 1] > 0) s.push(b[i + 1]);
      if (b[i + 2] > 0) s.push(b[i + 2]);
    }
    return s;
  }
}(),
'''
s=''
for pixel in img.getdata():
    r,g,b = pixel
    if r>0: s += chr(r)
    if g>0: s += chr(g)
    if b>0: s += chr(b)

with open(outfileraw, 'w') as f:
    f.write(s)

with open(outfilepretty, 'w') as f:
    json.dump(json.loads(s), f, indent=2)
