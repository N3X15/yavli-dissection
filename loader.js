/**
 * This file is (probably) copyright (c)2018 Yavli.com.
 *
 * I, Rob "N3X15" Nelson, make no claim to it.  It is provided here as an analysis sample.
 *
 * Comments have been added and the code has been "prettified" for ease of analysis.
 */
// This was all minified and shoved into a <script> at the bottom of the page.
if (oeo == undefined) {
  var oeo = {
    // The [r] means random string.  See ur().  Used to pull in current ad list from the Yavli proxy server.
    u: '//i.4cdn.org/scdfeolite/scdfenvaldite/2018/09/scdefieldlite-dragunow12857a-[r].png',
    // Timestamp...?  Unused.
    ts: '1537570641',
    // Shit to inject into the page if loading fails.  Empty, so unused. See: f()
    fl: [],
    // Whether to be a dick and blank all images if blocking is detected. Disabled at the moment.
    cf: false,
    // Unused.
    at_ru: "//orbitfour47.com/dan8e3.js",
    // Detects if orbitfour47.com is blocked. See abd().
    at_ri: "//orbitfour47.com/adclix.png",
    at_ei: "rightad", // Unused
    at_ec: "content-ad contentAd", // Unused
    at_es: "position:absolute;display:block;", // Unused
    at_e: true,
    at_r: true,
    i: false,
    c: 0,
    uc: false,
    cl: [],
    pv: false,
    css: false,
    sn: 0,
    start: function() {
      return function() {
        ! function(a, b, c) {
          a[b] = c()
        }(oeo, "v", function() {
          function a() {
            return {
              width: k(),
              height: l()
            }
          }

          function b(a, b) {
            var c = {};
            return b = +b || 0, c.width = (c.right = a.right + b) - (c.left = a.left - b), c.height = (c.bottom = a.bottom + b) - (c.top = a.top - b), c
          }

          function c(a, c) {
            return a = a && !a.nodeType ? a[0] : a, a && 1 === a.nodeType ? b(a.getBoundingClientRect(), c) : !1
          }

          function d(b) {
            b = null == b ? a() : 1 === b.nodeType ? c(b) : b;
            var d = b.height,
              e = b.width;
            return d = "function" == typeof d ? d.call(b) : d, e = "function" == typeof e ? e.call(b) : e, e / d
          }
          var e = {},
            f = "undefined" != typeof window && window,
            g = "undefined" != typeof document && document,
            h = g && g.documentElement,
            i = f.matchMedia || f.msMatchMedia,
            j = i ? function(a) {
              return !!i.call(f, a).matches
            } : function() {
              return !1
            },
            k = e.viewportW = function() {
              var a = h.clientWidth,
                b = f.innerWidth;
              return b > a ? b : a
            },
            l = e.viewportH = function() {
              var a = h.clientHeight,
                b = f.innerHeight;
              return b > a ? b : a
            };
          return e.mq = j, e.matchMedia = i ? function() {
            return i.apply(f, arguments)
          } : function() {
            return {}
          }, e.viewport = a, e.scrollX = function() {
            return f.pageXOffset || h.scrollLeft
          }, e.scrollY = function() {
            return f.pageYOffset || h.scrollTop
          }, e.rectangle = c, e.aspect = d, e.inX = function(a, b) {
            var d = c(a, b);
            return !!d && d.right >= 0 && d.left <= k()
          }, e.inY = function(a, b) {
            var d = c(a, b);
            return !!d && d.bottom >= 0 && d.top <= l()
          }, e.inViewport = function(a, b) {
            var d = c(a, b);
            return !!d && d.bottom >= 0 && d.right >= 0 && d.top <= l() && d.left <= k()
          }, e
        });
        ! function(n, t, e) {
          t[n] = e()
        }("q", oeo, function() {
          function n(n) {
            return [].slice.call(n, 0)
          }

          function t(n) {
            var t;
            return n && "object" == typeof n && (t = n.nodeType) && (1 == t || 9 == t)
          }

          function e(n) {
            return "object" == typeof n && isFinite(n.length)
          }

          function o(n) {
            for (var t = [], o = 0, r = n.length; r > o; ++o) e(n[o]) ? t = t.concat(n[o]) : t[t.length] = n[o];
            return t
          }

          function r(n) {
            var t, e, o = [];
            n: for (t = 0; t < n.length; t++) {
              for (e = 0; e < o.length; e++)
                if (o[e] == n[t]) continue n;
              o[o.length] = n[t]
            }
            return o
          }

          function i(n) {
            return n ? "string" == typeof n ? u(n)[0] : !n[s] && e(n) ? n[0] : n : f
          }

          function u(r, u) {
            var a, s = i(u);
            return s && r ? r === l || t(r) ? !u || r !== l && t(s) && m(r, s) ? [r] : [] : r && e(r) ? o(r) : f.getElementsByClassName && "string" == r && (a = r.match(c)) ? n(s.getElementsByClassName(a[1])) : r && (r.document || r.nodeType && 9 == r.nodeType) ? u ? [] : [r] : n(s.querySelectorAll(r)) : []
          }
          var c = /^\.([\w\-]+)$/,
            f = document,
            l = window,
            a = f.documentElement,
            s = "nodeType",
            m = "compareDocumentPosition" in a ? function(n, t) {
              return 16 == (16 & t.compareDocumentPosition(n))
            } : function(n, t) {
              return t = t == f || t == window ? a : t, t !== n && t.contains(n)
            };
          return u.uniq = r, u
        }, this);
        if (navigator.userAgent.search(" Brave/") > -1) oeo.at_r = true;
        if (oeo.i) return false;
        oeo.i = true;
        // Try to load adclix.png via Image().src
        return oeo.abd(function() {
          // Failed to load (onerror() called)
          // Inject things into page (doesn't on 4chan since oeo.fl=[])
          oeo.f(oeo.fl);
          // WIPE OUT ALL IMAGES. (sets them display: none !important)
          oeo.fc();
          // Unknown, function is empty.
          oeo.mf();
          // Inject shit into the page every 1.5s (again, it's empty)
          setTimeout(function() {
            oeo.f(oeo.fl);
          }, 1500);
          // Try loading ads via alternate method (the encoded image shit)
          oeo.now(oeo.ur(oeo.u), oeo.q("html")[0]);
          return true;
        });
      }
    }(),
    abd: function() {
      return function(f) {
        if (oeo.at_r) {
          var si = new Image();
          si.crossOrigin = "Anonymous";
          si.onerror = function(_si) {
            if (oeo.sn == 0) {
              oeo.sn = 1;
              return f();
            }
          };
          si.src = oeo.at_ri;
        }
      }
    }(),
    rc: function() {
      return function(a) {
        return a[Math.floor(Math.random() * a.length)];
      }
    }(),
    rq: function() {
      return function() {
        var sa = [];
        var wc = Math.floor(Math.random() * 12 + 1);
        for (var i = 0; i < wc; i++) {
          sa[i] = oeo.rs([1, 13]);
          sa[i] = sa[i].charAt(0).toUpperCase() + sa[i].substr(1);
        };
        return sa.join('-');
      }
    }(),
    rs: function() {
      return function(a) {
        var r = "",
          c = "abcdefghijklnopqrstuw0123456789";
        var l = 5,
          h = 12;
        if (a != undefined) {
          if (a.length >= 1) {
            var l = parseInt(a[0]),
              h = parseInt(a[0]);
          }
          if (a.length >= 2) {
            var h = parseInt(a[1]);
          }
          if (a.length == 3) {
            c = a[2];
          }
        }
        var le = Math.floor(Math.random() * (h - l + 1)) + l;
        for (i = 0; i < le; i++) r += c.charAt(Math.floor(Math.random() * c.length));
        return r;
      }
    }(),
    pad: function() {
      return function(v, a) {
        v = String(v);
        if (a != undefined) {
          l = parseInt(a[0]);
          v = (new Array(l).join('0') + v).slice(-l);
        }
        return v;
      }
    }(),
    ur: function() {
      return function(u, v) {
        var rx = /\[[a-zA-Z0-9:,-]+\]/g;
        var r = u.match(rx);
        if (r != null) {
          for (var i = 0; i < r.length; i++) {
            var n = r[i].replace('[', '').replace(']', '');
            var t = n.split(':');
            var key = t[0];
            var args = t[1];
            if (args != undefined) args = args.split(',');
            var value = r[i];
            // Format: [key:args=default, args...]
            // ex: [a:b=c] would mean key = a, args=[b], and if b isn't present, b=c

            // [r:min=5,max=12,pool='abcdefghijklnopqrstuw0123456789'] - Random string between min and max characters in length, using pool as the character pool.
            // ex: [r] -> "h6jw1ob94r2w"
            if (key == 'r') var value = oeo.rs(args);
            // [c:a,b,c,...] - Pick random string from list.
            // ex: [c:moot,hiro] -> "hiro"
            if (key == 'c') var value = oeo.rc(args);
            // [q] - Random string consisting of 1-12 blocks of random strings, each 1-13 characters in length, seperated by dashes.
            // ex: [q] -> "16hbd8et-S92s9j2e8j-6u-732sdd69-K-Cw141a-E-T2t41-20"
            if (key == 'q') var value = oeo.rq();
            // [i:width=0] - Zero-padded integer, fed the value of the 'i' key in v.
            // i is used to transmit which widget ID is displaying and which ads from that widget are displayed.
            // format: {widget}c{ad}c{ad}...-{widget}c{ad}c{ad}...
            if (key == 'i') var value = oeo.pad(v['i'], args);
            // [i:width=0] - Zero-padded integer, fed the value of the 'p' key in v.
            // Used to share oeo.pv, which appears to be a use-once thing. (0=has not rendered, 1=has rendered)
            if (key == 'p') var value = oeo.pad(v['p'], args);
            // [g:width=0] - Zero-padded integer, fed the value of the 'g' key in v.
            // Shares screen resolution in the form of a number from 1-5.  See g().
            if (key == 'g') var value = oeo.pad(v['g'], args);
            // [g:width=0] - Zero-padded integer, fed the value of the 'g' key in v.
            // Current widget ID.
            if (key == 'w') var value = oeo.pad(v['w'], args);
            u = u.replace(r[i], value);
          }
        }
        return u;
      }
    }(),
    fc: function() {
      if (oeo.cf) {
        var es = document.querySelectorAll("img[src*='wpengine.netdna-cdn.com']");
        for (var x = 0; x < es.length; x++) {
          es[x].outerHTML = es[x].outerHTML.replace(/[a-z0-9]+\.wpengine\.netdna-cdn\.com/gi, window.location.hostname).replace('display: none !important;', '');
        }
      }
    },
    mf: function() {},
    // Takes a list of entries in the form of:
    // {pattern: '.query selector #format', {action: 'inject', position: 'before|after', newHtml: '<html>...'}}
    f: function(fl) {
      for (var y = 0; y < fl.length; y++) {
        var k = fl[y];
        var es = document.querySelectorAll(k['pattern']);
        for (var x = 0; x < es.length; x++) {
          if (k['action'] == 'inject') {
            if (k['position'] == 'before') {
              var n = document.createElement("div");
              n.innerHTML = k['newHtml'];
              es[x].parentNode.insertBefore(n, es[x]);
              n.outerHTML = n.innerHTML;
            } else if (k['position'] == 'after') {
              var n = document.createElement("div");
              n.innerHTML = k['newHtml'];
              es[x].parentNode.insertBefore(n, es[x].nextSibling);
              n.outerHTML = n.innerHTML;
            }
          }
        }
      }
    },
    later: function() {
      var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;
      return function(o) {
        var obj = oeo.q(o.parent)[0];
        if (obj == undefined) {
          return false;
        }
        if (MutationObserver) {
          var obs = new MutationObserver(function(mutations, observer) {
            if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
              oeo.scan(o);
            }
          });
          obs.observe(obj, {
            childList: true,
            subtree: true,
          });
          oeo.scan(o);
        } else if (eventListenerSupported) {
          obj.addEventListener('DOMNodeInserted', oeo.scan, false);
        }
      }
    }(),
    scan: function() {
      return function(o) {
        var a = oeo.q(o.child, oeo.q(o.parent)[0]);
        for (var p = 0; p < a.length; p++) {
          if (oeo.cl[p] == undefined) {
            oeo.cl[p] = a[p];
            oeo.now(oeo.ur(oeo.u), a[p]);
          }
        }
      }
    }(),
    now: function() {
      return function(url, parent) {
        // Let's not do this twice. uc = loaded data
        if (oeo.uc != false) {
          // Process already collected ad data
          oeo.collection(oeo.uc, parent);
          // Not sure why they're doing this here but w/e.
          oeo.uc = false;
          return;
        }
        // Create an <img>
        var i = new Image();
        // Disable CORS
        i.crossOrigin = "Anonymous";
        // What we do once the image loads
        i.onload = function() {
          // How many fetches we've made.
          oeo.c++;
          // Create a <canvas>
          var c = document.createElement("canvas");
          // Create a 2D rendering context.
          var t = c.getContext("2d");
          // Get height and width from the image.
          var w = i.width;
          var h = i.height;
          // Set canvas to image size.
          c.style.width = c.width = w;
          c.style.height = c.height = h;
          // Disable alpha
          t.globalAlpha = 1.0;
          // No antialiasing?
          t.globalCompositeOperation = 'copy';
          // Draw the image onto the canvas.
          t.drawImage(i, 0, 0);
          // Get the image as a series of [r,g,b,a,r,g,b,a,...] bytes,
          // then convert each byte into an  integer and slap it into the end of the array.
          // It skips \0 bytes and the alpha byte.
          // (See p24())
          var b = oeo.p24(t.getImageData(0, 0, w, h).data);
          var s = "";
          // Now convert the array into a string, each byte being a character.
          for (var x = 0; x < b.length; x++)
            if (b[x]) s += String.fromCharCode(b[x]);
          // Parse string as JSON.
          var ucl = JSON.parse(s);
          // If first parsing and ucl.observe.enabled == 1, then we do mutation checks with MutationObserver.
          if (oeo.c == 1 && ucl.observe.enabled == 1) {
            oeo.uc = ucl;
            oeo.later(ucl.observe);
          } else {
            // Otherwise, we just render the ads.
            oeo.collection(ucl, parent);
          }
        };
        // The image itself.
        i.src = url;
      }
    }(),
    p24: function() {
      return function(b) {
        var s = [];
        for (var i = 0; i < b.length; i += 4) {
          if (b[i + 0] > 0) s.push(b[i + 0]);
          if (b[i + 1] > 0) s.push(b[i + 1]);
          if (b[i + 2] > 0) s.push(b[i + 2]);
        }
        return s;
      }
    }(),
    is_test: function() {
      return function() {
        if (window.location.hash.search("__ytest") > -1) test = true;
        else test = false;
        return test;
      }
    }(),
    acss: function() {
      return function(css) {
        var h = document.getElementsByTagName('head')[0],
          s = document.createElement('style');
        s.setAttribute("type", "text/css");
        if (s.styleSheet) s.styleSheet.cssText = css;
        else s.appendChild(document.createTextNode(css));
        return h.appendChild(s);
      }
    }(),
    g: function() {
      return function() {
        var w = window.innerWidth;
        var r = 5;
        if (w <= 479) r = 1;
        if (w <= 767) r = 2;
        if (w <= 1023) r = 3;
        if (w <= 1439) r = 4;
        return r;
      }
    }(),
    collection: function() {
      return function(ucl, parent) {
        var o = {
          init: function() {
            // Load CSS, if we're told to.
            if (oeo.css == false) {
              // Build element and fill it with css_common.
              oeo.css = oeo.acss(ucl.css_common);
            }
            // Load up each widget/unit.
            for (var i = 0; i < ucl.unit_list.length; i++) {
              o.unit(ucl.unit_list[i], parent);
            }
          },
          unit: function(u, parent) {
            // If we match a url in exclude_url_list, don't load unit.
            for (i = 0; i < u.exclude_url_list.length; i++) {
              var eu = new RegExp(u.exclude_url_list[i]);
              if (window.location.pathname.match(eu)) return;
            }

            // Load selector
            if (u.target_selector != undefined && u.target_selector != "") {
              var ts = u.target_selector,
                ta = u.target_approach,
                cs = u.container_style;
            // If __ytest is in the URL hash somewhere, and testing selectors are specified...
            } else if (oeo.is_test() && u.target_selector_test != undefined && u.target_selector_test != "") {
              // Replace standard selectors with these.
              var ts = u.target_selector_test,
                ta = u.target_approach_test,
                cs = u.container_style_test;
            } else return;
            // Add container css to css_specific.
            u.css_specific += "." + u.widget_class + " {" + u.container_style + "}";

            // Select ad container
            var t = oeo.q(ts, parent);

            // If we didn't find any, abort.
            if (t.length < 1) return;

            // Select first container.
            t = t[0];

            // Add container-specific CSS.
            oeo.css.textContent += u.css_specific;

            // If our action is before, inject HTML before specified contents.
            if (ta == "before") {
              var n = document.createElement("div");
              n.innerHTML = u.html;
              t.parentNode.insertBefore(n, t);
              n.outerHTML = n.innerHTML;
            // If our action is after, inject HTML after specified contents.
            } else if (ta == "after") {
              var n = document.createElement("div");
              n.innerHTML = u.html;
              t.parentNode.insertBefore(n, t.nextSibling);
              n.outerHTML = n.innerHTML;
            // If our action is append, append HTML
            } else if (ta == "append") t.innerHTML = t.innerHTML + u.html;
            else if (ta == "prepend") t.innerHTML = u.html + t.innerHTML;
            else if (ta == "fill") t.innerHTML = u.html;

            // Store widget in u.unit.
            u.unit = oeo.q("." + u.widget_class)[0];

            // Try rigging every second until it completes successfully, or until we fail 10 times.
            (function wait(tw) {
              if (tw == undefined) var tw = 0;
              else if (tw > 10) return;
              if (tw > 0 && o.rig(u) == true) return;
              setTimeout(function() {
                wait(tw + 1);
              }, 100);
            })();

            // Re-rig ads.
            window.addEventListener("resize", function() {
              o.rig(u);
            });
          },
          rig: function(u) {
            // Select image in widget.
            e = oeo.q("." + u.widget_class + " ." + u.img_class);

            // Fail if it's 0px wide. (Blocked or failed to load.)
            if (e[0].clientWidth == 0) return false;

            // Calculate desired image's position offset in the atlas.
            var ratio = e[0].clientWidth / ucl.sprite_width;
            var new_bg_height = Math.floor(ratio * ucl.sprite_height);
            var ad_height = Math.floor(new_bg_height / ucl.sprite_count);
            if (ad_height * ucl.sprite_count != new_bg_height) {
              new_bg_height = ad_height * ucl.sprite_count;
            }

            // Run-once test, per unit.
            if (u.once == undefined) {
              u.once = true;
              u.alv = [];
              for (var i = 0; i < e.length; i++) {
                var ad_id = e[i].className.split(u.img_class + "_")[1];
                oeo.css.textContent += '.' + u.widget_class + ' .' + u.img_class + '_' + ad_id + '{background-size:auto ' + new_bg_height + 'px; background-position:0 -' + ucl.ad_to_sprite[ad_id] * ad_height + 'px;}';
                if (e[i].offsetParent !== null) {
                  u.alv.push(ad_id);
                }
                var d = oeo.q("#" + e[i].parentElement.parentElement.parentElement.id + " " + e[i].parentElement.tagName);
                for (p = 0; p < d.length; p++) {
                  if (d[p].onclick == null) {
                    d[p].onclick = function(ee) {
                      var a = ee.target || ee.srcElement;
                      var ci = a.parentElement.getAttribute("ci");
                      window.location.assign(ucl.link_redirect_url + ucl.ad_to_link[ci]);
                      return false;
                    };
                  }
                }
              }(function rview() {
                /**
                 * rview()
                 *
                 * Called every 1s to tell the adserver which ads are in view, what resolution they're being displayed in, and the widget they're in.
                 */
                if (oeo.v.inViewport(u.unit)) {
                  var lt = "";
                  lt = u.widget_id + "c" + u.alv.join([separator = "c"]);
                  var pv = 0;
                  if (oeo.pv == false) {
                    oeo.pv = true;
                    pv = 1;
                  }
                  var i = new Image();
                  i.src = oeo.ur(ucl.view_url, {
                    'i': lt,
                    'p': pv,
                    'g': oeo.g(),
                    'w': u.widget_id
                  });
                  return;
                }
                setTimeout(function() {
                  rview();
                }, 1000);
              })();
            }
            return true;
          },
          /**
           * rload()
           *
           * Tells the adserver which ads were loaded.  Called after 1s after initialization to let CSS load ads.
           */
          rload: function() {
            //"load_url": "//i.4cdn.org/osqfeolite/osqfenvaldite/2018/09/serrual12857c[i]trigger-[r].png",
            // https://i.4cdn.org/mptfeolite/mptfenvaldite/2018/09/serrual12857c2156c9271c8377c9687trigger-w3e5jodtlb.png
            var ls = "";
            for (var i = 0; i < ucl.unit_list.length; i++) {
              if (ucl.unit_list[i]["alv"] != undefined) {
                ls += ucl.unit_list[i]["widget_id"] + "c" + ucl.unit_list[i]["alv"].join([separator = "c"]) + "-";
              }
            }
            // So i = [{widget_id:2156,alv:[9271,8377,9687]}]
            // Means For widget 2156, we've displayed ads 9271, 8377, and 9687.
            ls = ls.substr(0, ls.length - 1);
            var lsi = new Image();
            lsi.src = oeo.ur(ucl.load_url, {
              'i': ls,
              'g': oeo.g()
            });
            return true;
          },
        };
        o.init();
        setTimeout(o.rload, 1000);
        return o;
      }
    }()
  };
}
oeo.start();
