# Yavli Ad Proxy
*Sampled September 21st, 2018*

This is a dump and analysis of [Yavli](https://yavli.com), an ad distribution system and blocker bypass proxy, as implemented on 4chan.org, in order to display its capabilities and function.

## In short

Yavli is:
* Used in conjunction with a normal ad network (RevContent, at time of writing)
* A backup ad loading system, in case the primary loader fails or is blocked
* A proxy designed to get around CORS-based blockers (NoScript)
* A dick - `oeo.fc()` does attempt to hide all images on the site, poorly. (Only blocks images on boards.4chan.org because Hiro misconfigured it.)

Yavli is **not**:
* Very complicated, compared to [previous systems](https://gitlab.com/N3X15/argon-dissection).
* A bitcoin miner
* A virus
* A canvas fingerprinter\*

<sup>\*</sup> = It uses the canvas only to decode JSON hidden in images, and display ads. See [loader.js](loader.js).

## Full Analysis
A full analysis is available on [my blog](https://www.nexisonline.net/index.php/2018/09/20/4chans-new-ad-loader-and-canvas-fingerprinting-accusations/), with background information on the companies involved, and an operation walkthrough.
