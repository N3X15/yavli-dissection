# Fiddler2 Intercept Script

This script, when paired with Telerik Fiddler, will permit you to intercept the Yavli `<script>` tag and replace it with loader.modded.js.  This is useful for tracing how the software works.

***READ THESE INSTRUCTIONS CAREFULLY, OR YOU WILL BE UNABLE TO USE ANY SITE WITH SSL AND HSTS.***

## Setup

1. Install [Telerik Fiddler](https://www.telerik.com/fiddler) and run it.
2. *DISABLE CAPTURE* (File > Capture Traffic)
3. Enable SSL interception in options.
  1. Tools > Options
  2. SSL Tab
  3. *Check* Capture HTTPS CONNECTS
  4. *Check* Decrypt HTTPS Traffic
  5. *Check* Ignore certificate errors (unsafe)
  6. Actions > Trust Root Certificate
  7. Actions > Export Root Certificate to Desktop
4. Set up browsers.
  * Firefox:
    1. Open *Options*
    2. Select the *Privacy and Security* Tab
    3. Scroll to *Certificates* (near the bottom)
    4. Select *View Certificates*
    5. Select the *Authorities* Tab
    6. Hit *Import*
    7. Select the `FiddlerRoot.cer` file on your desktop.
    8. Complete importing it.
  * Chrome is covered by the *Trust Root Certificate* step above, as it uses system certificates and CAs.
5. Open the FiddlerScript tab in Fiddler.
6. Replace the contents with [fiddlerreplace.js](fiddlerreplace.js).
7. Ensure that `FILENAME_OF_YAVLI_LOADER` points to [loader.modded.js](loader.modded.js) on your hard drive.
8. Save Script.

## Loading a page
*Note: Instructions assume Firefox.  Adjust for your browser of choice.*

1. Disable all adblockers in your browser.
  * AdBlock Plus
  * &mu;Block Origin
  * NoScript
  * &mu;Matrix Origin
  * 4Chan X (includes ad blocking and CORS modifications, so yes, it has to go.)
2. Go to the thread or board index of your choice.
3. Hit F12 to open the browser's developer toolkit.
4. Open the networking tab to ensure it's recording.
5. Switch to Fiddler, and enable capture.
6. Switch back to your browser, and press CTRL+F5 to refresh the page, bypassing cache.
7. Disable capture in fiddler once the page has loaded.

## Cleaning up

1. Ensure Fiddler has stopped capturing
2. Remove CA from Firefox (should be called DO_NOT_TRUST > DO_NOT_TRUST_Fiddler)
3. Options > HTTPS > Actions > Remove Interception Certificates
4. Options > HTTPS > Actions > Reset Certificates (Removes system CA entry)
5. Re-enable ad blockers.
