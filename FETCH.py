from buildtools import log, http, os_utils

import os, sys, re, requests

REG_SCRIPT_DETECT=re.compile(r'<script>if\((?P<oeo>[a-z]+)==undefined\)\{var (?P=oeo)=\{(?P<u>[a-z]):\'(?P<u_val>//i.4cdn.org[^\']+)\',.+</script>')
DEFAULT_HEADERS={
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
}
res = requests.get('https://boards.4chan.org/diy/', headers=DEFAULT_HEADERS)
html = res.text
with open('diy.htm', 'w') as f:
    f.write(html)

m = REG_SCRIPT_DETECT.search(html)
if m is None:
    log.error('Could not locate Yavli in HTML.')
    sys.exit(1)

with log.info('Located Yavli at %d-%d (%dB)',m.start(),m.end(),(m.end()-m.start())):
    log.info('Yavli Var: %s', m.group('oeo'))
    log.info('Ad File Var: %s', m.group('u'))
    log.info('Ad File URL: %s', m.group('u_val'))

with open('loader.new.js', 'w') as f:
    f.write(m.group(0)[8:-9])

os_utils.cmd([os_utils.which('js-beautify'), '-f', 'loader.new.js', '-r', '-s', '2', '-q', '-n'], echo=True, show_output=True, critical=True)

http.DownloadFile('https:'+m.group('u_val').replace('[r]','abcdefg'), 'ad_data.png')

os_utils.cmd([sys.executable, 'decode_image.py', 'ad_data.png'], echo=True, show_output=True, critical=True)
os_utils.cmd([sys.executable, 'decode_json.py',  'ad_data.png.raw.json'], echo=True, show_output=True, critical=True)
